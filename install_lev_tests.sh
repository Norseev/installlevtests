#!/bin/sh

#
# Copyright© 2021 S.A.Norseev. All rights reserved.
#
# Contacts: norseev@gmail.com
#
# License: see accompanying file ReadMe.txt
#

#Если команда apt не поддерживается, замените ее на apt-get.

#временно повышаем свои привилегии до root
sudo su

apt install git --yes

#Компилятор С++
apt install g++ --yes

#Компиляторы mingw
apt install mingw-w64 --yes

#Для запуска приложений windows
apt install wine --yes

#Библиотеки для 32-битных приложений
apt install gcc-multilib --yes
apt install g++-multilib --yes

#Возвращаемся к правам обычного пользователя
exit

#Создаем каталоги
mkdir lev
mkdir lev_tests

#Скачиваем основной репозиторий LevClasses
cd lev
git init
git remote add lev https://bitbucket.org/Norseev/LevClasses
git fetch lev
git checkout master

#Скачиваем репозиторий тестов для LevClasses
cd ../lev_tests
git init
git remote add lev_tests https://bitbucket.org/Norseev/LevClasses_tests
git fetch lev_tests
git checkout master

cd ../
